package com.cita.wui.dummy;

import com.cita.wui.R;
import com.cita.wui.model.PaidPromo;
import com.cita.wui.model.Product;

public class Dummies {

	public static final String[] MAIN_SCREEN_TITLES = { "Hello Luke Skywalker",
			"HAN SOLO", "OBI-WAN KENOBI", "LEIA SKYWALKER", "CHEWBACCA",
			"R2-D2" };

	public static final Product[] products = {

			new Product(0, "Coke", "Calories 240\n" + "Total Fat 0g\n"
					+ "Sodium 75mg\n" + "Total Carbohydrate 65g", "10", 0.99,
					240, R.raw.coca),

			new Product(1, "Diet Coke", "Calories 3\n" + "Total Fat 0g\n"
					+ "Sodium 70mg\n" + "Total Carbohydrate 0g", "10", 0.99, 3,
					R.raw.diet_coke),

			new Product(1, "Snickers", "Calories 296\n" + "Total Fat 16.4g\n"
					+ "Sodium 250mg\n" + "Carbohydrates 34.5g", "10", 0.85,
					296, R.raw.snickers),
			new Product(1, "Lays Chips", "Calories 150\n" + "Total Fat 10.0g\n"
					+ "Sodium 150mg\n" + "Total Carbohydrate 15.0g", "10",
					0.99, 150, R.raw.lays),
			new Product(1, "Mountain Dew", "Calories 696\n" + "Total Fat 0g\n"
					+ "Sodium 252mg\n" + "Total Carbohydrate 184.8g", "10",
					0.99, 696, R.raw.md),
			new Product(1, "Oreo", "Calories 160\n" + "Total Fat 7g\n"
					+ "Sodium 190mg\n" + "Total Carbohydrate 24g", "10", 0.85,
					160, R.raw.oreo),
			new Product(1, "Skittles", "Calories 250\n" + "Total Fat 2.5g\n"
					+ "Sodium 10mg\n" + "Total Carbohydrate 56g", "10", 0.99,
					250, R.raw.skittles)

	};

	public static final PaidPromo[] promos = {
			new PaidPromo("Joe's Dinner", "50 % off on Pancakes.",
					R.drawable.promo2),
			new PaidPromo("Bar", "Happy Hour from 7:00 P.M. to 8:00 P.M.",
					R.drawable.promo3),
			new PaidPromo("Fancy Restaurant", "Dinner for 2, 50% off",
					R.drawable.promo4),
			new PaidPromo("Cooking School", "Free Cooking Lesson!",
					R.drawable.promo8)

	};

}
