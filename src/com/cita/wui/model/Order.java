package com.cita.wui.model;

import java.util.ArrayList;


public class Order {

	private ArrayList<Product> products;
	private String user;
	private int id;

	public Order() {

		products = new ArrayList<Product>();

	}

	public ArrayList<Product> getProducts() {
		return products;
	}

	public void setProducts(ArrayList<Product> products) {
		this.products = products;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double calculateTotalCost() {

		double total = 0;

		for (Product product : products) {
			total += product.getPrice();
		}

		return total;

	}

	public int calculateTotalCalories() {

		int total = 0;

		for (Product product : products) {
			total += product.getTotalCalories();
		}

		return total;

	}

	public String generateStringForNFC() {
		StringBuilder sb = new StringBuilder();

		for (Product product : products) {

			sb.append(product.getName());
			sb.append(":");
			sb.append(product.getPrice());
			sb.append(",");

		}

		return sb.toString();
	}

}
