package com.cita.wui.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BankTransaction implements Parcelable {

	// Members must be either primitives, primitive arrays or parcelables
	private String place;
	private String date;
	private Double amount;

	// TODO implement your constructors, getters & setters, methods
	public BankTransaction() {
	}

	public BankTransaction(String place, String date, Double amount) {
		super();
		this.place = place;
		this.date = date;
		this.amount = amount;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	private BankTransaction(Parcel in) {
		// Note: order is important - you must read in the same order
		// you write in writeToParcel!
		place = in.readString();
		date = in.readString();
		amount = in.readDouble();
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		// Note: order is important - you must write in the same order
		// you read in your private parcelable constructor!
		out.writeString(place);
		out.writeString(date);
		out.writeDouble(amount);

	}

	@Override
	public int describeContents() {
		// TODO return Parcelable.CONTENTS_FILE_DESCRIPTOR if your class members
		// include a FileDescriptor, otherwise you can simply return 0
		return 0;
	}

	public static final Parcelable.Creator<BankTransaction> CREATOR = new Parcelable.Creator<BankTransaction>() {
		public BankTransaction createFromParcel(Parcel in) {
			return new BankTransaction(in);
		}

		public BankTransaction[] newArray(int size) {
			return new BankTransaction[size];
		}
	};

}
