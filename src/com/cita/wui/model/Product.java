package com.cita.wui.model;

public class Product {

	private int id;
	private String name;
	private String description;
	private String disscount;
	private double price;
	private int totalCalories;
	private int image;

	public Product() {

	}

	public Product(int id, String name, String description, String disscount,
			int image) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.disscount = disscount;
		this.image = image;
	}

	public Product(int id, String name, String description, String disscount,
			double price, int totalCalories, int image) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.disscount = disscount;
		this.price = price;
		this.totalCalories = totalCalories;
		this.image = image;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisscount() {
		return disscount;
	}

	public void setDisscount(String disscount) {
		this.disscount = disscount;
	}

	public int getImage() {
		return image;
	}

	public void setImage(int image) {
		this.image = image;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getTotalCalories() {
		return totalCalories;
	}

	public void setTotalCalories(int totalCalories) {
		this.totalCalories = totalCalories;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", description="
				+ description + ", disscount=" + disscount + ", image=" + image
				+ "]";
	}

}
