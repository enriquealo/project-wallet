package com.cita.wui.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import android.os.Parcel;
import android.os.Parcelable;

@Root(name = "monederoWS")
public class Monedero {

        // TODO: Clarify DATA

        @Element(name = "estatus")
        private String status;

        @Element(name = "idMonedero")
        private int idMonedero;

        @Element(name = "idTipoMonedero")
        private int idMonederoType;

        @Element(name = "saldoTransferible")
        private double transferableCredit;

        @Element(name = "saldoIntransferible")
        private double untransferableCredit;

        @Element(name = "tipoMonedero")
        private String monederoType;

        public Monedero() {
        }

        public Monedero(String status, int idMonedero, int idMonederoType,
                        double transferableCredit, double untransferableCredit,
                        String monederoType) {
                super();
                this.status = status;
                this.idMonedero = idMonedero;
                this.idMonederoType = idMonederoType;
                this.transferableCredit = transferableCredit;
                this.untransferableCredit = untransferableCredit;
                this.monederoType = monederoType;
        }

        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }

        public int getIdMonedero() {
                return idMonedero;
        }

        public void setIdMonedero(int idMonedero) {
                this.idMonedero = idMonedero;
        }

        public int getIdMonederoType() {
                return idMonederoType;
        }

        public void setIdMonederoType(int idMonederoType) {
                this.idMonederoType = idMonederoType;
        }

        public double getTransferableCredit() {
                return transferableCredit;
        }

        public void setTransferableCredit(double transferableCredit) {
                this.transferableCredit = transferableCredit;
        }

        public double getUntransferableCredit() {
                return untransferableCredit;
        }

        public void setUntransferableCredit(double untransferableCredit) {
                this.untransferableCredit = untransferableCredit;
        }

        public String getMonederoType() {
                return monederoType;
        }

        public void setMonederoType(String monederoType) {
                this.monederoType = monederoType;
        }

        protected Monedero(Parcel in) {
                status = in.readString();
                idMonedero = in.readInt();
                idMonederoType = in.readInt();
                transferableCredit = in.readDouble();
                untransferableCredit = in.readDouble();
                monederoType = in.readString();
        }

        public int describeContents() {
                return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(status);
                dest.writeInt(idMonedero);
                dest.writeInt(idMonederoType);
                dest.writeDouble(transferableCredit);
                dest.writeDouble(untransferableCredit);
                dest.writeString(monederoType);
        }

        public static final Parcelable.Creator<Monedero> CREATOR = new Parcelable.Creator<Monedero>() {
                public Monedero createFromParcel(Parcel in) {
                        return new Monedero(in);
                }

                public Monedero[] newArray(int size) {
                        return new Monedero[size];
                }
        };
}