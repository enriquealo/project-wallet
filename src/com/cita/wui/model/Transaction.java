package com.cita.wui.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import android.os.Parcel;
import android.os.Parcelable;

@Root(name = "transaccionWS")
public class Transaction {

	// TODO: Clarify DATA

	@Element(name = "idTransaccion")
	private int id;

	@Element(name = "fecha")
	private String fecha;

	@Element(name = "monto")
	private Double amount;

	@Element(name = "realizadoPor")
	private String doneBy;

	@Element(name = "tipo")
	private String type;

	@Element(name = "usuario")
	private String user;

	public Transaction() {
	}

	public Transaction(int id, String fecha, Double amount, String doneBy,
			String type, String user) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.amount = amount;
		this.doneBy = doneBy;
		this.type = type;
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getDoneBy() {
		return doneBy;
	}

	public void setDoneBy(String doneBy) {
		this.doneBy = doneBy;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	protected Transaction(Parcel in) {
		id = in.readInt();
		fecha = in.readString();
		amount = in.readDouble();
		doneBy = in.readString();
		type = in.readString();
		user = in.readString();
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeString(fecha);
		dest.writeDouble(amount);
		dest.writeString(doneBy);
		dest.writeString(type);
		dest.writeString(user);
	}

	public static final Parcelable.Creator<Transaction> CREATOR = new Parcelable.Creator<Transaction>() {
		public Transaction createFromParcel(Parcel in) {
			return new Transaction(in);
		}

		public Transaction[] newArray(int size) {
			return new Transaction[size];
		}
	};
}