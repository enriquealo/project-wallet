package com.cita.wui.model;

import java.util.List;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "monederoWSs")
public class MonederoList {

        @ElementList(inline = true)
        public List<Monedero> monederos;

        public List<Monedero> getMonederos() {
                return monederos;
        }

        public void setMonederos(List<Monedero> monederos) {
                this.monederos = monederos;
        }

}