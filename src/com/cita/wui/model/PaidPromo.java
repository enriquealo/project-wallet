package com.cita.wui.model;

public class PaidPromo {

	String name;
	String Description;
	int resourceId;

	public PaidPromo() {

	}

	public PaidPromo(String name, String description, int resourceId) {
		super();
		this.name = name;
		Description = description;
		this.resourceId = resourceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public int getResourceId() {
		return resourceId;
	}

	public void setResourceId(int resourceId) {
		this.resourceId = resourceId;
	}

}
