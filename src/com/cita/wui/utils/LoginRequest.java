package com.cita.wui.utils;

import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.util.Log;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

/**
 * Created by enriqueramirez on 6/3/13.
 */
public class LoginRequest extends SpringAndroidSpiceRequest<String> {

	public LoginRequest() {
		super(String.class);
	}

	@Override
	public String loadDataFromNetwork() throws Exception {

		makeRequestSpringStyle();

		return "hello";

	}

	public int makeRequestSpringStyle() {

		int responseValue = 0;

		// Set the username and password for creating a Basic Auth request
		HttpAuthentication authHeader = new HttpBasicAuthentication("admin",
				"cambiame");
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setAuthorization(authHeader);

		// org.springframework.http.HttpEntity restEntity = new
		// org.springframework.http.HttpEntity<Object >(requestHeaders);

		HttpEntity<Object> requestEntity = new org.springframework.http.HttpEntity<Object>(
				requestHeaders);

		// Create a new RestTemplate Instance
		RestTemplate restTemplate = new RestTemplate();

		restTemplate.getMessageConverters().add(
				new StringHttpMessageConverter());

		try {

			// Make the HTTP GET request to the Basic Auth protected URL
			ResponseEntity<String> response = restTemplate.exchange(
					"http://cita.chi.itesm.mx/monedero/ws/usuario/autenticar",
					HttpMethod.POST, requestEntity, String.class);

			responseValue = response.getStatusCode().value();

			// Log.d("RESPONSE", response.getStatusCode().toString());

		} catch (Exception e) {

			Log.e("ERROR", e.getLocalizedMessage(), e);
			// Handle 401 Unauthorized response

		}

		return responseValue;

	}
}
