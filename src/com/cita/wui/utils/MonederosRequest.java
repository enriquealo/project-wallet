package com.cita.wui.utils;

import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.xml.SimpleXmlHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.net.Uri;
import android.util.Log;

import com.cita.wui.model.MonederoList;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class MonederosRequest extends SpringAndroidSpiceRequest<MonederoList> {

	public MonederosRequest() {
		super(MonederoList.class);
	}

	@Override
	public MonederoList loadDataFromNetwork() throws Exception {

		int responseValue = 0;

		// Authentication
		//TODO: Change user and password to shared preferences
		HttpAuthentication authHeader = new HttpBasicAuthentication("admin",
				"cambiame");
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setAuthorization(authHeader);

		HttpEntity<Object> requestEntity = new org.springframework.http.HttpEntity<Object>(
				requestHeaders);

		// Create a new RestTemplate Instance
		RestTemplate restTemplate = new RestTemplate();

		restTemplate.getMessageConverters().add(
				new SimpleXmlHttpMessageConverter());

		// restTemplate.getMessageConverters().add(
		// new StringHttpMessageConverter());

		Uri.Builder mBuilder = Uri.parse(
				"http://cita.chi.itesm.mx/monedero/ws/monedero/listar")
				.buildUpon();
		mBuilder.appendQueryParameter("usuario", "admin");

		String url = "";
		
		ResponseEntity<MonederoList> response = null;
		
		try {
			url = mBuilder.build().toString();
			// ResponseEntity<String> response = restTemplate.exchange(url,
			// HttpMethod.GET, requestEntity, String.class);

			 response = restTemplate.exchange(url,
					HttpMethod.GET, requestEntity, MonederoList.class);

			responseValue = response.getStatusCode().value();

			// Log.v("RESPONSE", responseValue + "");
			// Log.v("RESPONSE BODY",
			// response.getBody().getMonederos().size()+"");

		} catch (Exception e) {
			e.printStackTrace();

			Log.e("ERROR", e.getLocalizedMessage(), e);
			// Handle 401 Unauthorized response

		}

		// Log.v("URL", url);

		return response.getBody();
	}

}
