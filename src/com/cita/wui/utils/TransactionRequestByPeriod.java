package com.cita.wui.utils;

import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.xml.SimpleXmlHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.util.Log;

import com.cita.wui.model.TransactionList;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class TransactionRequestByPeriod extends
		SpringAndroidSpiceRequest<TransactionList> {

	Context context;
	String startingDate;
	String endingDate;

	public TransactionRequestByPeriod(Context context, String startingDate,
			String endingDate) {
		super(TransactionList.class);

		this.context = context;
		this.startingDate = startingDate;
		this.endingDate = endingDate;

	}

	@Override
	public TransactionList loadDataFromNetwork() throws Exception {

		int responseValue = 0;

		// TODO: Change user and password to shared preferences
		// Authentication headers
		HttpAuthentication authHeader = new HttpBasicAuthentication("admin",
				"cambiame");
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setAuthorization(authHeader);

		HttpEntity<Object> requestEntity = new HttpEntity<Object>(
				requestHeaders);

		// Create a new Rest template so the request can be done
		RestTemplate restTemplate = new RestTemplate();

		// use Simple XML Message Converter so the xml response can be parsed
		// into objects
		restTemplate.getMessageConverters().add(
				new SimpleXmlHttpMessageConverter());

		String url = "http://cita.chi.itesm.mx/monedero/ws/transaccion/listar?usuario="
				+ "admin"
				+ "&fechaInicio="
				+ startingDate
				+ "&fechaFin="
				+ endingDate;

		ResponseEntity<TransactionList> response = null;

		try {

			response = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, TransactionList.class);

			responseValue = response.getStatusCode().value();

			Log.v("RESPONSE VALUE", "" + responseValue);
			Log.v("RESPONSE BODY", response.getBody().getTransactions().size()
					+ "");

		} catch (Exception e) {
			Log.e("ERROR", e.getLocalizedMessage(), e);
			// Handle 401 Unauthorized response
		}

		return response.getBody();

	}
}
