package com.cita.wui.adapters;

import com.cita.wui.fragments.CreditCardFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class CreditCardFragmentAdapter extends FragmentPagerAdapter {

	public CreditCardFragmentAdapter(FragmentManager fm) {
		super(fm);

	}

	@Override
	public Fragment getItem(int position) {
		return new CreditCardFragment();
	}

	@Override
	public int getCount() {
		return 3;
	}
}
