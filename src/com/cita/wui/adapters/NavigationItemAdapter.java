package com.cita.wui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cita.wui.R;

public class NavigationItemAdapter extends BaseAdapter {

	private static final int ITEM_VIEW_TYPE_VIDEO = 0;
	private static final int ITEM_VIEW_TYPE_SEPARATOR = 1;
	private static final int ITEM_VIEW_TYPE_COUNT = 2;
	Context context;
	private Object[] OBJECTS;

	public static class SectionHeader {

		String title;
		int iconResource;

		public SectionHeader(String title, int iconResource) {
			this.title = title;
			this.iconResource = iconResource;
		}

	}

	public NavigationItemAdapter(Context context, Object[] OBJECTS) {
		this.context = context;
		this.OBJECTS = OBJECTS;

	}

	@Override
	public int getCount() {
		return OBJECTS.length;
	}

	@Override
	public Object getItem(int position) {
		return OBJECTS[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getViewTypeCount() {
		return ITEM_VIEW_TYPE_COUNT;
	}

	@Override
	public int getItemViewType(int position) {
		return (OBJECTS[position] instanceof SectionHeader) ? ITEM_VIEW_TYPE_SEPARATOR
				: ITEM_VIEW_TYPE_VIDEO;
	}

	@Override
	public boolean isEnabled(int position) {
		// A separator cannot be clicked !
		return getItemViewType(position) != ITEM_VIEW_TYPE_SEPARATOR;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final int type = getItemViewType(position);

		// First, let's create a new convertView if needed. You can also
		// create a ViewHolder to speed up changes if you want ;)
		if (convertView == null) {
			final LayoutInflater inflater = LayoutInflater.from(context);
			final int layoutID = type == ITEM_VIEW_TYPE_SEPARATOR ? R.layout.list_header_main_menu
					: R.layout.list_item_main_menu;
			convertView = inflater.inflate(layoutID, parent, false);
		}

		// We can now fill the list item view with the appropriate data.
		if (type == ITEM_VIEW_TYPE_SEPARATOR) {

			SectionHeader header = (SectionHeader) getItem(position);

			TextView txtSectionHeader = (TextView) convertView
					.findViewById(R.id.textview_navigation_header);

			txtSectionHeader.setText(header.title);

			ImageView mImageView = (ImageView) convertView
					.findViewById(R.id.imageview_list_item_navigation_header);

			mImageView.setImageResource(header.iconResource);

		} else {
			String item = (String) getItem(position);

			TextView txtTitle = (TextView) convertView.findViewById(R.id.textview_navigation_item);

			txtTitle.setText(item);

		}

		return convertView;
	}

}
