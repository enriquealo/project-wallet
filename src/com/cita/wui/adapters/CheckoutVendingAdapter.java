package com.cita.wui.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.cita.wui.PaymentActivity;
import com.cita.wui.R;
import com.cita.wui.model.Product;

public class CheckoutVendingAdapter extends ArrayAdapter<Product> {

	private final LayoutInflater mInflater;
	ArrayList<Product> products;
	Context context;

	public CheckoutVendingAdapter(Context context, int textViewResourceId,
			ArrayList<Product> products) {
		super(context, textViewResourceId, products);

		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.context = context;
		this.products = products;

	}

	public class ViewHolder {

		public TextView txtName;
		public Button btnDelete;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View rowView = convertView;

		if (rowView == null) {
			rowView = mInflater.inflate(R.layout.list_item_checkout, null);

			ViewHolder mHolder = new ViewHolder();

			mHolder.txtName = (TextView) rowView
					.findViewById(R.id.list_item_checkout_bought_item);
			mHolder.btnDelete = (Button) rowView
					.findViewById(R.id.list_item_checkout_button_remove_item);

			rowView.setTag(mHolder);

		}

		final ViewHolder mHolder = (ViewHolder) rowView.getTag();
		final Product product = products.get(position);

		mHolder.txtName.setText(product.getName());
		mHolder.btnDelete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Log.v("Removed", product.getName());

				products.remove(product);
				notifyDataSetChanged();
				PaymentActivity.refreshData();
			}
		});

		return rowView;
	}

}
