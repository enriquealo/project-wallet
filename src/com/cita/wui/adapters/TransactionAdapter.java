package com.cita.wui.adapters;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cita.wui.R;
import com.cita.wui.model.Transaction;

public class TransactionAdapter extends ArrayAdapter<Transaction> {

	private final LayoutInflater mInflater;

	Context context;

	List<Transaction> transactions;

	public TransactionAdapter(Context context, int textViewResourceId,
			List<Transaction> transactions) {

		super(context, textViewResourceId, transactions);

		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		this.context = context;
		this.transactions = transactions;

	}

	/**
	 * 
	 * Private class used for reusing views in a listview (makes listviews
	 * faster, like a lot)
	 * 
	 * */

	private class ViewHolder {

		public TextView txtTitle;
		public TextView txtSubtitle;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View rowView = convertView;

		if (rowView == null) {
			rowView = mInflater.inflate(R.layout.list_item_monedero, null);

			ViewHolder mHolder = new ViewHolder();

			mHolder.txtTitle = (TextView) rowView
					.findViewById(R.id.textview_list_item_monedero_title);
			mHolder.txtSubtitle = (TextView) rowView
					.findViewById(R.id.monedero_list_item_subtitle);

			rowView.setTag(mHolder);
		}

		ViewHolder mHolder = (ViewHolder) rowView.getTag();

		Transaction transaction = transactions.get(position);

		mHolder.txtTitle.setText(transaction.getType());
		Log.e("type", transaction.getType());
		mHolder.txtSubtitle.setText(transaction.getFecha());

		return rowView;
	}
}
