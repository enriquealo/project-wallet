package com.cita.wui.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cita.wui.R;
import com.cita.wui.WalletApplication;
import com.cita.wui.model.Order;
import com.cita.wui.model.Product;
import com.devspark.appmsg.AppMsg;

public class VendingIttemsAdapater extends ArrayAdapter<Product> {

	private final LayoutInflater mInflater;

	ArrayList<Product> products;
	Context context;

	Order mOrder;
	ArrayList<Product> orderProducts;

	public VendingIttemsAdapater(Context context, int textViewResourceId,
			ArrayList<Product> products) {
		super(context, textViewResourceId, products);

		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		this.context = context;
		this.products = products;

		// Getting order from application

		WalletApplication app = (WalletApplication) ((Activity) context)
				.getApplication();

		mOrder = app.getOrder();
		orderProducts = mOrder.getProducts();

	}

	public class ViewHolder {

		// Main Data
		public ImageView imgIcon;
		public TextView txtItemName;
		public TextView txtItemPrice;

		// Expandable Area
		public TextView txtProductDescription;
		public TextView txtCalories;

		// Button add to cart
		public Button btnAddToCart;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View rowView = convertView;

		if (rowView == null) {
			rowView = mInflater.inflate(R.layout.list_item_vending, null);

			ViewHolder mHolder = new ViewHolder();

			// Main area
			mHolder.imgIcon = (ImageView) rowView
					.findViewById(R.id.list_item_vending_image_product);

			mHolder.txtItemName = (TextView) rowView
					.findViewById(R.id.list_item_vending_header);

			mHolder.txtItemPrice = (TextView) rowView
					.findViewById(R.id.list_item_vending_price);

			// Expandable Area

			mHolder.txtProductDescription = (TextView) rowView
					.findViewById(R.id.list_item_vending_txt_product_description);

			mHolder.txtCalories = (TextView) rowView
					.findViewById(R.id.list_item_vending__txt_product_calories);

			mHolder.btnAddToCart = (Button) rowView
					.findViewById(R.id.list_item_vending_button_add_to_cart);

			rowView.setTag(mHolder);

		}

		ViewHolder mHolder = (ViewHolder) rowView.getTag();

		final Product product = products.get(position);

		mHolder.txtItemName.setText(product.getName());
		mHolder.txtItemPrice.setText("$ " + product.getPrice());
		mHolder.txtCalories.setText(product.getDescription());
		mHolder.imgIcon.setImageDrawable(context.getResources().getDrawable(
				product.getImage()));

		mHolder.btnAddToCart.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				AppMsg.Style style = AppMsg.STYLE_CONFIRM;

				orderProducts.add(product);

				AppMsg appMsg = AppMsg.makeText((Activity) context,
						product.getName() + " Added To Cart", style);

				appMsg.show();

			}
		});

		return rowView;

	}

}
