package com.cita.wui.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.cita.wui.R;
import com.cita.wui.fragments.MonederoFragment;
import com.cita.wui.fragments.TransactionsFragment;
import com.cita.wui.fragments.TransactionsPeriodFragment;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one
 * of the sections/tabs/pages.
 */
public class LocalWalletSectionsPagerAdapter extends FragmentPagerAdapter {

	Context mContext;

	public LocalWalletSectionsPagerAdapter(FragmentManager fm, Context context) {
		super(fm);

		mContext = context;

	}

	@Override
	public Fragment getItem(int position) {

		MonederoFragment monederoFragment = new MonederoFragment();
		TransactionsPeriodFragment transactionsPeriodFragment = new TransactionsPeriodFragment();
		TransactionsFragment mTransactionsFragment = new TransactionsFragment();

		switch (position) {
		case 0:

			return monederoFragment;

		case 1:
			return mTransactionsFragment;

		case 2:
			return transactionsPeriodFragment;

		default:
			break;
		}
		return null;

	}

	@Override
	public int getCount() {
		// Show total pages.
		return 3;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return mContext.getResources().getString(
					R.string.local_wallets_section_header_my_wallets);
		case 1:
			return mContext.getResources().getString(
					R.string.local_wallets_section_header_recent_transactions);

		case 2:
			return mContext.getResources().getString(
					R.string.local_wallets_section_header_search_by_period);

		}
		return null;
	}
}
