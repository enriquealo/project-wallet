package com.cita.wui.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cita.wui.R;
import com.cita.wui.model.Monedero;

public class MonederoAdapter extends ArrayAdapter<Monedero> {

	private final LayoutInflater mInflater;

	Context context;

	List<Monedero> monederos;

	public MonederoAdapter(Context context, int textViewResourceId,
			List<Monedero> monederos) {
		super(context, textViewResourceId, monederos);

		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		this.context = context;
		this.monederos = monederos;

	}

	/**
	 * 
	 * Private class used for reusing views in a listview (makes listviews
	 * faster, like a lot)
	 * 
	 * */

	private class ViewHolder {

		public TextView txtTitle;
		public TextView txtSubtitle;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View rowView = convertView;

		if (rowView == null) {
			rowView = mInflater.inflate(R.layout.list_item_monedero, null);

			ViewHolder mHolder = new ViewHolder();

			mHolder.txtTitle = (TextView) rowView
					.findViewById(R.id.textview_list_item_monedero_title);
			mHolder.txtSubtitle = (TextView) rowView
					.findViewById(R.id.monedero_list_item_subtitle);

			rowView.setTag(mHolder);
		}

		ViewHolder mHolder = (ViewHolder) rowView.getTag();

		Monedero monedero = monederos.get(position);

		mHolder.txtTitle.setText(monedero.getMonederoType());

		mHolder.txtSubtitle.setText("Saldo transferible: $ "
				+ monedero.getTransferableCredit()
				+ "\nSaldo Intransferible: $ "
				+ monedero.getUntransferableCredit());

		return rowView;
	}
}
