package com.cita.wui;

import com.cita.wui.model.Order;

import android.app.Application;
import android.content.Context;

public class WalletApplication extends Application {

	static Context context;

	public Order order;

	@Override
	public void onCreate() {

		context = this;
		order = new Order();

		super.onCreate();
	}

	public static Context getContext() {

		return context;

	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

}
