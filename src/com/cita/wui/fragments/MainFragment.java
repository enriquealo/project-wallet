package com.cita.wui.fragments;

import android.app.Activity;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.cita.wui.R;
import com.cita.wui.adapters.FeaturedPromosAdapter;
import com.cita.wui.adapters.NavigationItemAdapter;
import com.cita.wui.adapters.NavigationItemAdapter.SectionHeader;
import com.cita.wui.listListeners.NavigationClickListener;
import com.sherlock.navigationdrawer.compat.SherlockActionBarDrawerToggle;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Activities that
 * contain this fragment must implement the
 * {@link MainFragment.OnMainFragmentInteractionListener} interface to handle
 * interaction events. Use the {@link MainFragment#newInstance} factory method
 * to create an instance of this fragment.
 * 
 */
public class MainFragment extends SherlockFragment {

	private OnMainFragmentInteractionListener mListener;

	/**
	 * Use this factory method to create a new instance of this fragment using
	 * the provided parameters.
	 * 
	 * @return A new instance of fragment MainFragment.
	 */
	// Drawer containing the side menu stuff
	private DrawerLayout mDrawerLayout;

	private ActionBarHelper mActionbar;

	private SherlockActionBarDrawerToggle mDrawerToggle;

	private ListView navigationList;

	private ListView promosList;

	public static MainFragment newInstance() {
		MainFragment fragment = new MainFragment();

		return fragment;
	}

	public MainFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		setHasOptionsMenu(true);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.fragment_main, container,
				false);
		// Referencing DrawerLayout
		mDrawerLayout = (DrawerLayout) rootView
				.findViewById(R.id.layout_main_fragment_drawer_main_container);

		mActionbar = createActionBarHelper();
		mActionbar.init();

		// ActionBarDrawerToggle provides convenient helpers for tying together
		// the
		// prescribed interactions between a top-level sliding drawer and the
		// action bar.

		mDrawerToggle = new SherlockActionBarDrawerToggle(getActivity(),
				mDrawerLayout, R.drawable.ic_drawer,
				R.string.main_fragment_drawer_open,
				R.string.main_fragment_drawer_closed);

		mDrawerToggle.syncState();

		Object[] OBJECTS = {
				new SectionHeader(
						getStringFromResource(R.string.navigation_title_banking),
						R.drawable.ic_nav_card),
				getStringFromResource(R.string.navigation_banking_payments),
				new SectionHeader(
						getStringFromResource(R.string.navigation_title_clever),
						R.drawable.ic_nav_money),
				getStringFromResource(R.string.navigation_clever_vending),
				getStringFromResource(R.string.navigation_clever_transactions),
				getStringFromResource(R.string.navigation_clever_parking),
				getStringFromResource(R.string.navigation_clever_food),
				getStringFromResource(R.string.navigation_clever_stationary),
				new SectionHeader(
						getStringFromResource(R.string.navigation_title_school),
						R.drawable.ic_nav_books),
				getStringFromResource(R.string.navigation_school_grades),
				getStringFromResource(R.string.navigation_school_attendance),
				getStringFromResource(R.string.navigation_school_facilities),
				new SectionHeader(
						getStringFromResource(R.string.navigation_title_tui),
						R.drawable.ic_nav_bag),
				getStringFromResource(R.string.navigation_tui_explore),
				getStringFromResource(R.string.navigation_tui_near),
				getStringFromResource(R.string.navigation_tui_ranking),
				getStringFromResource(R.string.navigation_tui_favourite) };

		// NavigationListview
		navigationList = (ListView) rootView
				.findViewById(R.id.listview_main_fragment_navigation);

		NavigationItemAdapter navAdapter = new NavigationItemAdapter(
				getActivity(), OBJECTS);

		navigationList.setAdapter(navAdapter);

		// Handle clicks on the navigation panel
		navigationList.setOnItemClickListener(new NavigationClickListener(
				getActivity()));

		// PromosListView
		promosList = (ListView) rootView
				.findViewById(R.id.listview_main_fragment_promos);

		FeaturedPromosAdapter mPromosAdapter = new FeaturedPromosAdapter(
				getActivity());

		promosList.setAdapter(mPromosAdapter);
		
		return rootView;
	}

	/***
	 * Gets A String from the resource folder
	 * 
	 * @param stringResource
	 *            . number of the resource from the R.java file
	 * @return String from the strings.xml file
	 * 
	 * **/
	private String getStringFromResource(int stringResource) {

		String result = "";
		result = getActivity().getResources().getString(stringResource);

		return result;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		/*
		 * The action bar home/up action should open or close the drawer.
		 * mDrawerToggle will take care of this.
		 */
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		return super.onOptionsItemSelected(item);

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed(Uri uri) {
		if (mListener != null) {
			mListener.onFragmentInteraction(uri);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnMainFragmentInteractionListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnMainFragmentInteractionListener {
		// TODO: Update argument type and name
		public void onFragmentInteraction(Uri uri);
	}

	/**
	 * A drawer listener can be used to respond to drawer events such as
	 * becoming fully opened or closed. You should always prefer to perform
	 * expensive operations such as drastic relayout when no animation is
	 * currently in progress, either before or after the drawer animates.
	 * 
	 * When using ActionBarDrawerToggle, all DrawerLayout listener methods
	 * should be forwarded if the ActionBarDrawerToggle is not used as the
	 * DrawerLayout listener directly.
	 */
	@SuppressWarnings("unused")
	private class DemoDrawerListener implements DrawerLayout.DrawerListener {
		@Override
		public void onDrawerOpened(View drawerView) {
			mDrawerToggle.onDrawerOpened(drawerView);
			mActionbar.onDrawerOpened();
		}

		@Override
		public void onDrawerClosed(View drawerView) {
			mDrawerToggle.onDrawerClosed(drawerView);
			mActionbar.onDrawerClosed();
		}

		@Override
		public void onDrawerSlide(View drawerView, float slideOffset) {
			mDrawerToggle.onDrawerSlide(drawerView, slideOffset);
		}

		@Override
		public void onDrawerStateChanged(int newState) {
			mDrawerToggle.onDrawerStateChanged(newState);
		}
	}

	/**
	 * Create a compatible helper that will manipulate the action bar if
	 * available.
	 */
	private ActionBarHelper createActionBarHelper() {
		return new ActionBarHelper();
	}

	/**
	 * Create a compatible helper that will manipulate the action bar if
	 * available.
	 */

	private class ActionBarHelper {
		private final ActionBar mActionBar;
		private CharSequence mDrawerTitle;
		private CharSequence mTitle;

		private ActionBarHelper() {
			mActionBar = ((SherlockFragmentActivity) getActivity())
					.getSupportActionBar();
		}

		public void init() {
			mActionBar.setDisplayHomeAsUpEnabled(true);
			mActionBar.setHomeButtonEnabled(true);
			mTitle = mDrawerTitle = getActivity().getTitle();
		}

		/**
		 * When the drawer is closed we restore the action bar state reflecting
		 * the specific contents in view.
		 */
		public void onDrawerClosed() {
			mActionBar.setTitle(mTitle);
		}

		/**
		 * When the drawer is open we set the action bar to a generic title. The
		 * action bar should only contain data relevant at the top level of the
		 * nav hierarchy represented by the drawer, as the rest of your content
		 * will be dimmed down and non-interactive.
		 */
		public void onDrawerOpened() {
			mActionBar.setTitle(mDrawerTitle);
		}
	}

}
