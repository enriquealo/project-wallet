package com.cita.wui.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.cita.wui.R;
import com.cita.wui.adapters.TransactionAdapter;
import com.cita.wui.model.TransactionList;
import com.cita.wui.utils.TransactionRequestByPeriod;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.XmlSpringAndroidSpiceService;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class TransactionsPeriodFragment extends ListFragment {

	private SpiceManager contentManager = new SpiceManager(
			XmlSpringAndroidSpiceService.class);

	TransactionAdapter mAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_transactions_period,
				container, false);

		Button startDateButton = (Button) rootView
				.findViewById(R.id.button_fragment_transaction_start_date);
		Button endDateButton = (Button) rootView
				.findViewById(R.id.button_fragment_transaction_end_date);

		Button btnSearch = (Button) rootView
				.findViewById(R.id.button_fragment_transaction_search);

		startDateButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				showDatePickerDialog("start");

			}
		});

		endDateButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				showDatePickerDialog("end");

			}
		});

		btnSearch.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				performRequest();

			}
		});

		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();

		contentManager.start(getActivity());

	}

	@Override
	public void onStop() {
		super.onStop();

		contentManager.shouldStop();

	}

	private void performRequest() {

		if (mAdapter != null) {
			mAdapter.clear();
			mAdapter.notifyDataSetChanged();
		}

		SharedPreferences settings = getActivity().getSharedPreferences(
				"Settings", Context.MODE_PRIVATE);

		String startingDate = settings.getString("start", "");
		String endingDate = settings.getString("end", "");

		TransactionRequestByPeriod request = new TransactionRequestByPeriod(
				getActivity(), startingDate, endingDate);
		contentManager.execute(request, new TransactionListRequestListener());

	}

	public void showDatePickerDialog(String tag) {

		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getFragmentManager(), tag);

	}

	/**
	 * Request for transactions
	 * 
	 * */

	private class TransactionListRequestListener implements
			RequestListener<TransactionList> {

		@Override
		public void onRequestFailure(SpiceException e) {
			Log.v("ERROR", "Error during request" + e.getMessage());

		}

		@Override
		public void onRequestSuccess(TransactionList transactions) {

			mAdapter = new TransactionAdapter(getActivity(),
					R.layout.list_item_transaction,
					transactions.getTransactions());

			setListAdapter(mAdapter);

		}

	}

}