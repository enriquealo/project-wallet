package com.cita.wui.fragments;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cita.wui.R;
import com.cita.wui.adapters.TransactionAdapter;
import com.cita.wui.model.Transaction;
import com.cita.wui.model.TransactionList;
import com.cita.wui.utils.TransactionRequest;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.XmlSpringAndroidSpiceService;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class TransactionsFragment extends ListFragment {

	private SpiceManager contentManager = new SpiceManager(
			XmlSpringAndroidSpiceService.class);

	TransactionAdapter mAdapter;

	public static TransactionList mTransactionList;
	List<Transaction> mTransactions;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_transactions,
				container, false);

		performRequest();

		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();
		contentManager.start(getActivity());
	}

	@Override
	public void onStop() {
		contentManager.shouldStop();
		super.onStop();
	}

	/**
	 * Request stuff
	 * 
	 * */

	private void performRequest() {

		TransactionRequest request = new TransactionRequest();
		contentManager.execute(request, new TransactionListRequestListener());

	}

	private class TransactionListRequestListener implements
			RequestListener<TransactionList> {

		@Override
		public void onRequestFailure(SpiceException e) {
			Log.v("ERROR", "Error during request" + e.getMessage());

		}

		@Override
		public void onRequestSuccess(TransactionList transactions) {

			mAdapter = new TransactionAdapter(getActivity(),
					R.layout.list_item_transaction,
					transactions.getTransactions());

			setListAdapter(mAdapter);

		}

		// end of request stuff
	}
}
