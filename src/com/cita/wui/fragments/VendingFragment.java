package com.cita.wui.fragments;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cita.wui.R;
import com.cita.wui.adapters.VendingIttemsAdapater;
import com.cita.wui.dummy.Dummies;
import com.cita.wui.model.Product;
import com.tjerkw.slideexpandable.library.SlideExpandableListAdapter;

@SuppressLint("NewApi")
public class VendingFragment extends android.app.ListFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_vending, container,
				false);

		ArrayList<Product> products = new ArrayList<Product>();

		for (Product product : Dummies.products) {
			products.add(product);
			Log.v("Product", product.getName() + " " + product.getPrice());
		}

		VendingIttemsAdapater mAdapater = new VendingIttemsAdapater(
				getActivity(), R.layout.list_item_vending, products);

		setListAdapter(new SlideExpandableListAdapter(mAdapater,
				R.id.list_item_vending_vending_layout_card,
				R.id.list_item_vending_vending_layout_expandable));

		return rootView;
	}

}
