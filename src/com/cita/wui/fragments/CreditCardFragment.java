package com.cita.wui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cita.wui.R;

public class CreditCardFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_bank_card,
				container, false);

		return rootView;

	}

	public static CreditCardFragment newInstance() {

		CreditCardFragment fragment = new CreditCardFragment();

		return fragment;

	}
}
