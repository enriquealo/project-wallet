package com.cita.wui.fragments;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cita.wui.R;
import com.cita.wui.adapters.MonederoAdapter;
import com.cita.wui.model.Monedero;
import com.cita.wui.model.MonederoList;
import com.cita.wui.utils.MonederosRequest;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.XmlSpringAndroidSpiceService;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class MonederoFragment extends ListFragment {

	private SpiceManager contentManager = new SpiceManager(
			XmlSpringAndroidSpiceService.class);

	MonederoAdapter mAdapter;

	MonederoList mMonederosList;
	List<Monedero> mMonederos;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_monedero, container,
				false);

		mMonederosList = new MonederoList();
		mMonederos = new ArrayList<Monedero>();

		mAdapter = new MonederoAdapter(getActivity(),
				R.layout.list_item_monedero, mMonederos);

		setListAdapter(mAdapter);

		performRequest();

		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();
		contentManager.start(getActivity());
	}

	@Override
	public void onStop() {
		contentManager.shouldStop();
		super.onStop();
	}

	/**
	 * Request stuff
	 * 
	 * */

	private void performRequest() {

		MonederosRequest request = new MonederosRequest();
		contentManager.execute(request, new MonederoListRequestListener());

	}

	private class MonederoListRequestListener implements
			RequestListener<MonederoList> {

		@Override
		public void onRequestFailure(SpiceException e) {
			Log.v("ERROR", "Error during request" + e.getMessage());

		}

		@Override
		public void onRequestSuccess(MonederoList monederos) {

			ArrayList<Monedero> mMonederos = new ArrayList<Monedero>();

			mMonederos = (ArrayList<Monedero>) monederos.getMonederos();

			for (Monedero monedero : mMonederos) {
				mAdapter.add(monedero);
				mAdapter.notifyDataSetChanged();
			}

		}

	}

	// end of request stuff

}
