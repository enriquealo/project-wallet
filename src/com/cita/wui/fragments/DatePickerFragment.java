package com.cita.wui.fragments;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Button;
import android.widget.DatePicker;

import com.cita.wui.R;

public class DatePickerFragment extends DialogFragment implements
		DatePickerDialog.OnDateSetListener {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current date as the default date in the picker
		final Calendar c = Calendar.getInstance();

		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		// Create a new instance of DatePickerDialog and return it
		return new DatePickerDialog(getActivity(), this, year, month, day);
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {

		getActivity();
		SharedPreferences settings = getActivity().getSharedPreferences(
				"Settings", Context.MODE_PRIVATE);

		SharedPreferences.Editor editor = settings.edit();

		String day = "";
		String month = "";

		if (dayOfMonth <= 9) {
			day = "0" + dayOfMonth;

		} else {

			day = "" + dayOfMonth;

		}

		if (monthOfYear <= 9) {

			month = "0" + (monthOfYear + 1);

		} else {
			month = "" + (monthOfYear + 1);
		}

		StringBuilder date = new StringBuilder().append(day).append("/")
				.append(month).append("/").append(year);

		String startOrEnd = "";

		if (getTag().equals("start")) {
			startOrEnd = "start";

			Button btn = (Button) getActivity().findViewById(
					R.id.button_fragment_transaction_start_date);
			btn.setText(date);

		} else {
			startOrEnd = "end";
			Button btn = (Button) getActivity().findViewById(
					R.id.button_fragment_transaction_end_date);
			btn.setText(date);
		}

		editor.putString(startOrEnd, date.toString());

		editor.commit();

		// Log.v("DATE", date.toString());
	}
}