package com.cita.wui.listListeners;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.cita.wui.BankingActivity;
import com.cita.wui.LocalWalletActivity;
import com.cita.wui.VendingActivity;

public class NavigationClickListener implements OnItemClickListener {

	Context context;

	public NavigationClickListener(Context context) {
		this.context = context;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, final View view,
			int position, long id) {

		Log.v("NAVIGATION ITEM", "" + position);
		
		Intent mIntent = null;
		
		switch (position) {

		case 1:
			mIntent = new Intent(context, BankingActivity.class);
			break;
		case 3:
			// TODO: Vending Intent
			mIntent = new Intent(context, VendingActivity.class);
			
			break;
		case 4:
			mIntent = new Intent(context, LocalWalletActivity.class);

			break;
		case 5:
			// TODO: Parking lot intent
			break;
		case 6:
			// TODO: Food Services intent
			break;
		case 7:
			// TODO: Stationary Store intent
			break;
		case 9:
			// TODO: Grades Intent
			break;
		case 10:
			// TODO: Atendance Intent
			break;
		case 11:
			// TODO: Facilities Intent
			break;
		case 13:
			// TODO: Explore Intent
			break;
		case 14:
			// TODO: Near Intent
			break;
		case 15:
			// TODO: Ranking Intent
			break;
		case 16:
			// TODO: Favourites Intent
			break;

		default:
			break;

		}
		
		context.startActivity(mIntent);

	}
}
