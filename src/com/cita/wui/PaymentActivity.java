package com.cita.wui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.cita.wui.adapters.CheckoutVendingAdapter;
import com.cita.wui.model.Order;
import com.cita.wui.model.Product;

public class PaymentActivity extends Activity {

	TextView txtCredit;
	static TextView txtTotal;
	TextView txtCalories;
	static TextView txtTotalCalories;
	static Order mOrder;
	ArrayList<Product> products;

	CheckoutVendingAdapter mAdapter;
	ListView mList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_payment);

		final android.app.ActionBar actionBar = getActionBar();
		// Show the Up button in the action bar.
		actionBar.setDisplayHomeAsUpEnabled(true);

		WalletApplication app = (WalletApplication) getApplication();

		mOrder = app.getOrder();

		products = mOrder.getProducts();

		txtCredit = (TextView) findViewById(R.id.activity_payment_txt_credit);

		txtTotal = (TextView) findViewById(R.id.activity_payment_txt_total_amount_value);
		txtTotalCalories = (TextView) findViewById(R.id.activity_payment_txt_total_calories_value);

		txtTotal.setText("$ " + mOrder.calculateTotalCost());
		txtTotalCalories.setText("" + mOrder.calculateTotalCalories());

		// TODO: Connect to the shared preferences
		txtCredit.setText("Available: $100.00");

		mList = (ListView) findViewById(R.id.listCheckout);

		mAdapter = new CheckoutVendingAdapter(getApplicationContext(),
				R.layout.list_item_checkout, products);

		mList.setAdapter(mAdapter);

		txtTotal.setText("$ " + mOrder.calculateTotalCost());
		txtTotalCalories.setText("" + mOrder.calculateTotalCalories());

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.payment, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			break;
		}
		return true;
	}

	public void goPayment(View v) {

		Intent mIntent = new Intent(this, SendingPaymentActivity.class);
		startActivity(mIntent);

	}

	public static void refreshData() {
		txtTotal.setText("$ " + mOrder.calculateTotalCost());
		txtTotalCalories.setText("" + mOrder.calculateTotalCalories());
	}

}
